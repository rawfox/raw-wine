# raw-wine

wine-4.21 (Staging)
Build from 2. December 2019

- Build Typ ..........: WOW64 
- Threads to build on : 12 
- Staging args .......: --all 
- 32-Bit args ........: --without-xml2 --without-xslt --without-gnutls 
- 64-Bit args ........: --enable-win64 
- Install path .......: /home/osboxes/src/raw-wine/raw-wine 

Includes the official fix for SetThreadDescription().
https://bugs.winehq.org/show_bug.cgi?id=48157

